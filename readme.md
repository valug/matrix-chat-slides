# A Matrix Chat VALUG presentation

A presentation held at a [VALUG](https://valug.at/) meeting.

Requirements for build:
- [Make](https://www.gnu.org/software/make/manual/make.html)
- [Landslide](https://github.com/adamzap/landslide/)

Build:
- Just run `make`
- The output gets generated into the `output/presentation.html` file
