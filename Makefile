OUTDIR=output
IMAGES=$(wildcard images/*)
OUTPUTS=$(OUTDIR)/presentation.html

all: $(OUTPUTS)

clean:
	rm -f $(OUTPUTS)

$(OUTDIR)/%.html: %.md $(IMAGES)
	landslide $< -i -d $@
	cp $< $(OUTDIR)/$<

.PHONY: all clean
