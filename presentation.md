# Matrix Chat

## An open standard for decentralised persistent communication

An overview

2017-01-13

[VALUG.at](https://valug.at/)

Wolfgang Silbermayr

Source code: <https://gitlab.com/valug/matrix-chat-slides>

License of the presentation: [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

![CC-BY-SA 4.0](images/cc-by-sa.png)

---

# Oveview

- Screenshots
- Availability
- Description
- Structural overview
- Video and Voice Chat
- Clients
- Servers
- Federation
- Encryption
- Bots
- Bridges
- APIs
- Outlook

---

# Screenshots Web-Client

![Riot Web Client](images/web-client.scaled.png)

---

# Screenshots Android-Client

![Riot Android Client](images/android-client.scaled.png)

---

# Availability (Riot)

<https://riot.im/>

- Browser: <https://riot.im/app/>
- Browser developer version: <https://riot.im/develop/>
- Anrdoid: [F-Droid](https://f-droid.org/repository/browse/?fdid=im.vector.alpha), [Play Store](https://play.google.com/store/apps/details?id=im.vector.alpha)
- iOS: [App Store](https://itunes.apple.com/us/app/vector.im/id1083446067)
- [Desktop](https://riot.im/desktop.html) (electron app):
    - Debian/Ubuntu (repo, `/etc/apt/sources.list.d/riot.list`):
        - `deb https://riot.im/packages/debian/ <distribution> main`
    - Windows
    - MacOS

---

# Availability (other clients)

<http://matrix.org/docs/projects/try-matrix-now.html>

We focus on Riot, because is the most used and most feature-complete client.

---

# Description

"Matrix is an open standard for decentralised communication, providing simple HTTP APIs
and open source reference implementations for securely distributing and persisting JSON
over an open federation of servers."
(from the [Matrix website](https://matrix.org/))

---

# Structural overview

![Structure](images/structure.png)

---

# Video and Voice Chat

Voice (and video) over Matrix uses the WebRTC 1.0 standard to transfer call media
(i.e. the actual voice and video traffic). Matrix is used to signal the establishment
and termination of the call by sending call events, like any other event. Currently
calls are only supported in rooms with exactly two participants - however, one of those
participants may be a conferencing bridge. We’re looking at better ways to do group calling.
(from the [Matrix website FAQ](http://matrix.org/docs/guides/faq.html))

---

# Clients

Many clients are available, but Riot is currently the most advanced.

See <https://matrix.org/docs/projects/try-matrix-now.html> for the complete
list.

The encryption is supported by Riot only. The WeeChat plugin has a
preliminary implementation which is no longer compatible with the final
specification.

---

# Servers

The reference implementation, and the software running on the matrix.org instance is
[Synapse](https://github.com/matrix-org/synapse), written in Python.

There are several other server projects. One of the most promising is an
implementation in Rust, called [Ruma](https://github.com/ruma/ruma).

---

# Federation

See the interactive example on [How does it work?](https://matrix.org/#about).

TLS is used by default.

---

# Encryption

Matrix end-to-end encryption is called [Megolm](https://matrix.org/docs/spec/megolm.html).

"The Megolm ratchet is intended for encrypted messaging applications where there may
be a large number of recipients of each message, thus precluding the use of peer-to-peer
encryption systems such as Olm.

It also allows a recipient to decrypt received messages multiple times. For instance, in
client/server applications, a copy of the ciphertext can be stored on the (untrusted)
server, while the client need only store the session keys."

Encryption is implemented in Riot since November 2016, so still very new. Several
user-visible problems have occurred in real-world usage, and are being tackled down
at the moment.

---

# Bots

Bots appear normal users, but are running on the server. They
can be added to a room from the client interface.

The matrix.org instance has a few bots installed:

- Github
- Giphy
- Guggy
- RSS
- Travis CI

---

# Bridges

Unlike bots, bridges are not real users. They need to be supported
by the server instance.

The matrix.org instance has a few bridges installed:

- IRC
- Gitter
- Slack

---

# APIs

The API documentation can be found on <https://matrix.org/docs/spec/>

- Client-Server API: Interaction between clients and servers (matrix chat programs, bots)
- Server-Server API: Federation between servers
- Application Service API: Privileged server plugins (used for bridges)
- Identity Service API: Mapping of third party IDs to Matrix IDs
- Push Gateway API:  Push notifications for Matrix events

---

# Outlook

- Matrix is gaining users quite fast right now
- It's still a moving target, and will stay for some more time
- Development is progressing, most grave user-facing are being fixed at the moment
- Some features in the backlog:
    - [Better E2EE key verification (2142)](https://github.com/vector-im/riot-web/issues/2142)
    - [Cross-signing devices (2714)](https://github.com/vector-im/riot-web/issues/2714)
    - [Threaded view (2349)](https://github.com/vector-im/riot-web/issues/2349)
    - [Export/Import current E2EE state (2108)](https://github.com/vector-im/riot-web/issues/2108)
    - [Recheck device list notification (2564)](https://github.com/vector-im/riot-web/issues/2564)
    - [Decryption of room history (2286)](https://github.com/vector-im/riot-web/issues/2286)
